package com.viapolares.openmoviegraph.data.repository

import com.viapolares.openmoviegraph.data.TvShow
import com.viapolares.openmoviegraph.data.TvShowDetails
import io.reactivex.Single

/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */
interface TvShowRepository{
    fun getTvShowsPagedLiveData(): TvShowsPagedResult<TvShow>
    fun getTvShowDetails(id: String): Single<TvShowDetails>
}