package com.viapolares.openmoviegraph.data

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Stanislav Sydorenko (Stanislav.Sydorenko@gmail.com)
 */

interface TvShowApi {

    @GET("tv/popular")
    fun getPopularTvShowList(@Query("page") pageNumber: Int,
                             @Query("api_key") apiKey: String = THE_MOVIE_DB_API_KEY): Single<TvShowResponse>

    @GET("tv/{tv_id}")
    fun getTvShowDetails(@Path("tv_id") tvShowId: String,
                         @Query("api_key") apiKey: String = THE_MOVIE_DB_API_KEY): Single<TvShowDetails>
}