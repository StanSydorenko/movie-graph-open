package com.viapolares.openmoviegraph.data.paging

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import android.arch.paging.PageKeyedDataSource
import com.viapolares.openmoviegraph.data.TvShow
import com.viapolares.openmoviegraph.data.TvShowApi
import com.viapolares.openmoviegraph.data.TvShowResponse
import com.viapolares.openmoviegraph.data.repository.TvShowRepositoryImpl

/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */
class PagedContentSourceFactory(private val api: TvShowApi): DataSource.Factory<Int, TvShow>(){

    val sourceLiveData = MutableLiveData<PagedContentDataSource>()

    override fun create(): DataSource<Int, TvShow>{
        val source = PagedContentDataSource(api)
        sourceLiveData.postValue(source)
        return source
    }
}

class PagedContentDataSource(private val api: TvShowApi): PageKeyedDataSource<Int, TvShow>() {

    val initialRequestState = MutableLiveData<RequestState>()
    val nextRequestState = MutableLiveData<RequestState>()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, TvShow>) {
        initialRequestState.postValue(RequestState(Status.LOADING))
        api.getPopularTvShowList(TvShowRepositoryImpl.INITIAL_PAGE)
                .subscribe({
                    initialRequestState.postValue(RequestState(Status.LOADED))
                    callback.onResult(it.results, null, getNextPageNumber(it))
                }, {
                    initialRequestState.postValue(RequestState(Status.ERROR))
                })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, TvShow>) {
        nextRequestState.postValue(RequestState(Status.LOADING))
        api.getPopularTvShowList(params.key)
                .subscribe({
                    nextRequestState.postValue(RequestState(Status.LOADED))
                    callback.onResult(it.results,  getNextPageNumber(it))
                }, {
                    nextRequestState.postValue(RequestState(Status.ERROR))
                })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, TvShow>) {
        // ignored, since we only ever append to initial load for now
    }

    private fun getNextPageNumber(response: TvShowResponse): Int?{
        return if (response.page < response.totalPages) response.page + 1 else null
    }
}