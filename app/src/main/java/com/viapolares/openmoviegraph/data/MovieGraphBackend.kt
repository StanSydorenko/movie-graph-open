package com.viapolares.openmoviegraph.data

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Stanislav Sydorenko (Stanislav.Sydorenko@gmail.com)
 */

const val THE_MOVIE_DB_API_KEY = "5747b8b3ce857a4acec723b0313aab2b"

const val THE_MOVIE_DB_API_VERSION = 3

const val THE_MOVIE_DB_BASE_URL = "https://api.themoviedb.org/$THE_MOVIE_DB_API_VERSION"

interface MovieGraphBackend {
    fun build(): Retrofit
}

class MovieGraphBackendImpl: MovieGraphBackend {

    override fun build(): Retrofit = Retrofit.Builder()
            .baseUrl("$THE_MOVIE_DB_BASE_URL/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
}