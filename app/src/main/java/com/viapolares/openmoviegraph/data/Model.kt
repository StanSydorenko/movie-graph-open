package com.viapolares.openmoviegraph.data

import com.google.gson.annotations.SerializedName

/**
 * Created by Stanislav Sydorenko (Stanislav.Sydorenko@gmail.com)
 */

data class TvShowResponse(@SerializedName("page") val page: Int,
                          @SerializedName("total_results") val totalResults: Int,
                          @SerializedName("total_pages") val totalPages: Int,
                          @SerializedName("results") val results: List<TvShow>)

data class TvShow(val id: String,
                  @SerializedName("name") val title: String,
                  @SerializedName("poster_path") val posterPath: String?,
                  @SerializedName("vote_average") val voteAverage: Float)

data class TvShowDetails(val id: String,
                         @SerializedName("name") val title: String?,
                         @SerializedName("backdrop_path") val backdropPath: String?,
                         @SerializedName("poster_path") val posterPath: String?,
                         val overview: String?,
                         @SerializedName("first_air_date") val releaseDate: String?,
                         @SerializedName("number_of_episodes") val  episodesNumber: Int?,
                         @SerializedName("number_of_seasons") val  seasonsNumber: Int?,
                         @SerializedName("vote_average") val voteAverage: Float?,
                         @SerializedName("vote_count") val  voteCount: Int?)
