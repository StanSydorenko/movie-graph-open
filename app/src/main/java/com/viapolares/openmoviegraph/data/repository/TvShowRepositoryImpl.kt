package com.viapolares.openmoviegraph.data.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations.switchMap
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.viapolares.openmoviegraph.data.TvShow
import com.viapolares.openmoviegraph.data.TvShowApi
import com.viapolares.openmoviegraph.data.TvShowDetails
import com.viapolares.openmoviegraph.data.paging.PagedContentSourceFactory
import com.viapolares.openmoviegraph.data.paging.RequestState
import io.reactivex.Single

/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */
data class TvShowsPagedResult<T>(val pagedList: LiveData<PagedList<T>>,
                                 val initialRequestState: LiveData<RequestState>,
                                 val nextRequestState: LiveData<RequestState>)

class TvShowRepositoryImpl(private val api: TvShowApi, private val dataSourceFactory: PagedContentSourceFactory) : TvShowRepository {

    override fun getTvShowsPagedLiveData(): TvShowsPagedResult<TvShow> {
        val livePagedList = LivePagedListBuilder<Int, TvShow>(dataSourceFactory, defaultPageConfig()).build()
        val initialRequestState = switchMap(dataSourceFactory.sourceLiveData) { it.initialRequestState }
        val nextRequestState = switchMap(dataSourceFactory.sourceLiveData) { it.nextRequestState }
        return TvShowsPagedResult(livePagedList, initialRequestState, nextRequestState)
    }

    override fun getTvShowDetails(id: String): Single<TvShowDetails> {
        return api.getTvShowDetails(id)
    }

    companion object {
        const val INITIAL_PAGE = 1
        private const val DEFAULT_PAGE_SIZE = 20

        fun defaultPageConfig(): PagedList.Config =
                PagedList.Config.Builder()
                        .setPageSize(DEFAULT_PAGE_SIZE)
                        .build()
    }
}

