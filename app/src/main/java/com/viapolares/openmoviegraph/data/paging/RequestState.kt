package com.viapolares.openmoviegraph.data.paging

/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */
enum class Status {
    LOADING,
    LOADED,
    ERROR
}

data class RequestState(val status: Status, val msg: String? = null)