package com.viapolares.openmoviegraph.data

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Stanislav Sydorenko (Stanislav.Sydorenko@gmail.com)
 */
@Module
class DataModule {

    @Singleton
    @Provides
    fun provideMovieGraphApi(): MovieGraphBackend =
            MovieGraphBackendImpl()

    @Singleton
    @Provides
    fun provideTvShowService(backend: MovieGraphBackend): TvShowApi =
            backend.build().create(TvShowApi::class.java)

}