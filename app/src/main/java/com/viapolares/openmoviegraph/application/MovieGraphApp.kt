package com.viapolares.openmoviegraph.application

import com.viapolares.openmoviegraph.application.dagger.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

/**
 * Created by Stanislav Sydorenko (Stanislav.Sydorenko@gmail.com)
 */
class MovieGraphApp: DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<MovieGraphApp> =
            DaggerAppComponent.builder().create(this)
}