package com.viapolares.openmoviegraph.application.dagger

import com.viapolares.openmoviegraph.ui.activity.details.TvShowDetailsActivity
import com.viapolares.openmoviegraph.ui.activity.list.TvShowListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */
@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector
    abstract fun contributeTvShowListFragmentInjector(): TvShowListFragment

    @ContributesAndroidInjector
    abstract fun contributeTvShowDetailsActivityInjector(): TvShowDetailsActivity
}