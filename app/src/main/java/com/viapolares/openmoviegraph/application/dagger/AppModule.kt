package com.viapolares.openmoviegraph.application.dagger

import android.content.Context
import android.content.SharedPreferences
import com.viapolares.openmoviegraph.application.MovieGraphApp
import com.viapolares.openmoviegraph.data.DataModule
import com.viapolares.openmoviegraph.ui.ViewModelModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */
@Module(includes = [(ViewModelModule::class), (DataModule::class), (ActivitiesModule::class)])
class AppModule {

    @Singleton
    @Provides
    fun provideContext(app: MovieGraphApp): Context = app.applicationContext

    @Singleton
    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferences
            = context.getSharedPreferences(context.packageName + "_pref", Context.MODE_PRIVATE)
}