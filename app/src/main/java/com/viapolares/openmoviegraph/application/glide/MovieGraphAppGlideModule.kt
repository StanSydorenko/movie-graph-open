package com.viapolares.openmoviegraph.application.glide

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by Stanislav Sydorenko (Stanislav.Sydorenko@gmail.com)
 */
@GlideModule
class MovieGraphAppGlideModule : AppGlideModule()