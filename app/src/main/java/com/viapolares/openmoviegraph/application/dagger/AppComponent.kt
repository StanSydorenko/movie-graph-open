package com.viapolares.openmoviegraph.application.dagger

import com.viapolares.openmoviegraph.application.MovieGraphApp
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by Stanislav Sydorenko (Stanislav.Sydorenko@gmail.com)
 */
@Singleton
@Component(modules = [
    (AndroidSupportInjectionModule::class),
    (AppModule::class)])
interface AppComponent: AndroidInjector<MovieGraphApp> {

    @Component.Builder
    abstract class Builder: AndroidInjector.Builder<MovieGraphApp>()
}