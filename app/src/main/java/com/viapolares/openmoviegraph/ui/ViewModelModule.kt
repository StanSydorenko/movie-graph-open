package com.viapolares.openmoviegraph.ui

import com.viapolares.openmoviegraph.data.TvShowApi
import com.viapolares.openmoviegraph.data.paging.PagedContentSourceFactory
import com.viapolares.openmoviegraph.data.repository.TvShowRepository
import com.viapolares.openmoviegraph.data.repository.TvShowRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


/**
 * Created by Stanislav Sydorenko (Stanislav.Sydorenko@gmail.com)
 */
@Module
class ViewModelModule {

    @Singleton
    @Provides
    fun providePagedContentSourceFactory(api: TvShowApi) : PagedContentSourceFactory =
            PagedContentSourceFactory(api)

    @Singleton
    @Provides
    fun provideContentRepository(api: TvShowApi, factory: PagedContentSourceFactory) : TvShowRepository =
            TvShowRepositoryImpl(api, factory)

    @Singleton
    @Provides
    fun provideViewModelFactory(repository: TvShowRepository) : ViewModelFactory =
            ViewModelFactory(repository)
}