package com.viapolares.openmoviegraph.ui

import android.content.Context
import android.content.res.Configuration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.view.WindowManager
import android.widget.ImageView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.viapolares.openmoviegraph.R
import com.viapolares.openmoviegraph.application.glide.GlideApp

/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */

fun ImageView.loadWithGlide(path: String?, placeholderResId: Int = R.drawable.logo_placeholder_portrait){
    GlideApp.with(this)
            .load(path)
            .placeholder(placeholderResId)
            .error(placeholderResId)
            .transition(DrawableTransitionOptions.withCrossFade())
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(this)
}

fun isTablet(context: Context): Boolean {
    // Check if screen size greater than 6.5"
    val dm = getDisplayMetrics(context)
    val d = Math.sqrt((dm.heightPixels / dm.ydpi * (dm.heightPixels / dm.ydpi) + dm.widthPixels / dm.xdpi * (dm.widthPixels / dm.xdpi)).toDouble())
    return d >= 6.5
}

fun getDisplayMetrics(context: Context): DisplayMetrics {
    val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val displayMetrics = DisplayMetrics()
    wm.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics
}

fun RecyclerView.setAdjustableLayoutManager(){
    val isLandscapeMode = resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE
    layoutManager = if (isTablet(context)) {
        GridLayoutManager(context, if (isLandscapeMode) TABLET_LAND_COLUMNS else TABLET_PORT_COLUMNS)
    } else {
        GridLayoutManager(context, if (isLandscapeMode) PHONE_LAND_COLUMNS else PHONE_PORT_COLUMNS)
    }
}

private const val PHONE_PORT_COLUMNS = 3
private const val PHONE_LAND_COLUMNS = 5
private const val TABLET_PORT_COLUMNS = 4
private const val TABLET_LAND_COLUMNS = 6