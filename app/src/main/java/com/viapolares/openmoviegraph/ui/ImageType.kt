package com.viapolares.openmoviegraph.ui

/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */

enum class ImageType(private val width: Int){
    POSTER(185),
    BACKDROP(780),
    PROFILE(185);

    fun getUrl(path: String) = "http://image.tmdb.org/t/p/w$width/$path"
}