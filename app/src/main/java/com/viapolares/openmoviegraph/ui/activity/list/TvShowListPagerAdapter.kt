package com.viapolares.openmoviegraph.ui.activity.list

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */
class TvShowListPagerAdapter(fragmentManager: FragmentManager): FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int) = TvShowListFragment()

    override fun getCount() = TvShowListPage.values().size

    override fun getPageTitle(position: Int) = TvShowListPage.values()[position].name
}

enum class TvShowListPage{
    NEW, TOP_RATED, POPULAR
}