package com.viapolares.openmoviegraph.ui.activity.onboarding


import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Intent
import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.transition.ChangeBounds
import android.support.transition.TransitionManager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.animation.AnticipateOvershootInterpolator
import com.viapolares.openmoviegraph.R
import com.viapolares.openmoviegraph.ui.activity.list.TvShowListActivity
import com.viapolares.openmoviegraph.ui.activity.onboarding.view.SatelliteView
import kotlinx.android.synthetic.main.activity_onboarding.*
import kotlinx.android.synthetic.main.activity_onboarding_secondary_satellites.*


class OnboardingActivity : AppCompatActivity() {

    private var currentStep = OnBoardingStep.FIRST.ordinal

    private val satelliteViews = mutableListOf<SatelliteView>()
    private val normalConstraintSet = ConstraintSet()
    private lateinit var reelAnim: ValueAnimator

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme_NoActionBar)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)

        act_onboarding__view_pager.adapter = OnBoardingPagerAdapter()

        handlePageIndicator()
        act_onboarding__satellite_image_1.isCurrent = true
        satelliteViews.add(OnBoardingStep.FIRST.ordinal, act_onboarding__satellite_image_1)
        satelliteViews.add(OnBoardingStep.SECOND.ordinal, act_onboarding__satellite_image_2)
        satelliteViews.add(OnBoardingStep.THIRD.ordinal, act_onboarding__satellite_image_3)
        satelliteViews.add(act_onboarding__satellite_secondary_image_1)
        satelliteViews.add(act_onboarding__satellite_secondary_image_2)
        satelliteViews.add(act_onboarding__satellite_secondary_image_3)
        satelliteViews.add(act_onboarding__satellite_secondary_image_4)
        satelliteViews.add(act_onboarding__satellite_secondary_image_5)

        reelAnim = createReelAnim()

        act_onboarding__btn_forward.setOnClickListener { if (!reelAnim.isRunning){ moveForward() } }
        act_onboarding__btn_backward.setOnClickListener { if (!reelAnim.isRunning){ moveBackward() } }
    }

    private fun createReelAnim(): ObjectAnimator{
        val animator = ObjectAnimator.ofFloat(act_onboarding__film_reel, View.ROTATION, 0f)
        animator.duration = DURATION_MAIN_MS
        animator.interpolator = AnticipateOvershootInterpolator(0.3f)
        return animator
    }

    private fun moveForward(){
        if (currentStep == OnBoardingStep.SUMMARY.ordinal){
            startActivity(Intent(this, TvShowListActivity::class.java))
            return
        }
        currentStep += 1
        if (currentStep == OnBoardingStep.SUMMARY.ordinal){
            toSummaryMode()
        } else {
            act_onboarding__view_pager.currentItem = currentStep
            satelliteViews.forEachIndexed { index, satelliteView ->
                satelliteView.onForwardClicked(index == currentStep)
            }
            reelAnim.setFloatValues(0f, ONE_TURN_DEGREE * 2)
            reelAnim.start()
        }
        handlePageIndicator()
        handleStepButtons()
    }

    private fun moveBackward(){
        if (currentStep == OnBoardingStep.SUMMARY.ordinal){
            fromSummaryMode()
            currentStep -= 1
        } else if (currentStep != OnBoardingStep.FIRST.ordinal){
            currentStep -= 1
            act_onboarding__view_pager.currentItem = currentStep
            satelliteViews.forEachIndexed { index, satelliteView ->
                satelliteView.onBackwardClicked(index == currentStep)
            }
            reelAnim.setFloatValues(0f, - ONE_TURN_DEGREE * 2)
            reelAnim.start()
        }
        handlePageIndicator()
        handleStepButtons()
    }

    private fun handlePageIndicator(){
        act_onboarding__page_indicator_1.isChecked = currentStep == OnBoardingStep.FIRST.ordinal
        act_onboarding__page_indicator_2.isChecked = currentStep == OnBoardingStep.SECOND.ordinal
        act_onboarding__page_indicator_3.isChecked = currentStep == OnBoardingStep.THIRD.ordinal
        act_onboarding__page_indicator_4.isChecked = currentStep == OnBoardingStep.SUMMARY.ordinal
    }

    private fun handleStepButtons(){
        act_onboarding__btn_backward.visibility =
                if (currentStep != OnBoardingStep.FIRST.ordinal){ View.VISIBLE } else { View.GONE }

        act_onboarding__btn_forward.text = if (currentStep == OnBoardingStep.SUMMARY.ordinal){
            getString(R.string.onboarding_btn_finish)
        } else {
            getString(R.string.onboarding_btn_next)
        }
    }

    private fun toSummaryMode(){
        normalConstraintSet.clone(act_onboarding__root)
        val summaryConstraintSet = ConstraintSet()
        summaryConstraintSet.clone(this, R.layout.activity_onboarding_summary)
        val transition = ChangeBounds()
        transition.interpolator = AnticipateOvershootInterpolator(0.5f)
        transition.duration = DURATION_MAIN_MS
        TransitionManager.beginDelayedTransition(act_onboarding__root, transition)
        summaryConstraintSet.applyTo(act_onboarding__root)
    }

    private fun fromSummaryMode(){
        val transition = ChangeBounds()
        transition.interpolator = AnticipateOvershootInterpolator(0.5f)
        transition.duration = DURATION_MAIN_MS
        TransitionManager.beginDelayedTransition(act_onboarding__root, transition)
        normalConstraintSet.applyTo(act_onboarding__root)
    }

    companion object {
        const val DURATION_MAIN_MS = 1000L
        const val DURATION_SCALE_MS = 300L
        const val DURATION_SCALE_DELAY_MS = DURATION_MAIN_MS - DURATION_SCALE_MS
        const val ONE_TURN_DEGREE = 360f
        const val ANGLE_STEP = ONE_TURN_DEGREE.div(3).plus(ONE_TURN_DEGREE) // 120 + 360
    }
}
