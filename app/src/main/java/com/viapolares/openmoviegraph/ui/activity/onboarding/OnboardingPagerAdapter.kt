package com.viapolares.openmoviegraph.ui.activity.onboarding

import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.viapolares.openmoviegraph.R
import kotlinx.android.synthetic.main.view_onboarding.view.*

/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */
class OnBoardingPagerAdapter: PagerAdapter(){

    override fun instantiateItem(collection: ViewGroup, position: Int): Any {
        val ctx = collection.context
        val step = OnBoardingStep.values()[position]
        val layout: View = LayoutInflater.from(ctx).inflate(R.layout.view_onboarding, collection, false)
        layout.view_onboarding__title.text = ctx.resources.getString(step.titleResId)
        layout.view_onboarding__subtitle.text = ctx.resources.getString(step.subtitleResId)
        collection.addView(layout)
        return layout
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

    override fun isViewFromObject(view: View, `object`: Any) = view === `object`

    override fun getCount() = OnBoardingStep.values().size
}

enum class OnBoardingStep(val titleResId: Int, val subtitleResId: Int){
    FIRST(R.string.act_onboarding_page_1_title, R.string.act_onboarding_page_1_subtitle),
    SECOND(R.string.act_onboarding_page_2_title, R.string.act_onboarding_page_2_subtitle),
    THIRD(R.string.act_onboarding_page_3_title, R.string.act_onboarding_page_3_subtitle),
    SUMMARY(R.string.act_onboarding_page_1_title, R.string.act_onboarding_page_1_subtitle)
}