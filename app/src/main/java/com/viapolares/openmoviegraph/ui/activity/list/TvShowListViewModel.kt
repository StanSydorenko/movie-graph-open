package com.viapolares.openmoviegraph.ui.activity.list

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import com.viapolares.openmoviegraph.data.TvShow
import com.viapolares.openmoviegraph.data.paging.RequestState
import com.viapolares.openmoviegraph.data.repository.TvShowRepository
import com.viapolares.openmoviegraph.data.repository.TvShowsPagedResult

/**
 * Created by Stanislav Sydorenko (Stanislav.Sydorenko@gmail.com)
 */
class TvShowListViewModel(private val repository: TvShowRepository): ViewModel(){

    private var repoPagedResult: TvShowsPagedResult<TvShow>? = null

    val tvShows: LiveData<PagedList<TvShow>>? = getRepoResult()?.pagedList
    val initialRequestState: LiveData<RequestState>? = getRepoResult()?.initialRequestState
    val nextRequestState: LiveData<RequestState>? = getRepoResult()?.nextRequestState

    private fun getRepoResult(): TvShowsPagedResult<TvShow>? {
        if (repoPagedResult == null){
            repoPagedResult = repository.getTvShowsPagedLiveData()
        }
        return repoPagedResult
    }
}