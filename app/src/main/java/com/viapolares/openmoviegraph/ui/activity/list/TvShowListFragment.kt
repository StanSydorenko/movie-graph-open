package com.viapolares.openmoviegraph.ui.activity.list

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.SharedElementCallback
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.viapolares.openmoviegraph.R
import com.viapolares.openmoviegraph.data.paging.Status
import com.viapolares.openmoviegraph.ui.ViewModelFactory
import com.viapolares.openmoviegraph.ui.setAdjustableLayoutManager
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.frg_video_list.*
import javax.inject.Inject

/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */
class TvShowListFragment: DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.frg_video_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        frg_video_list_recycler_view.setAdjustableLayoutManager()
        val pagedAdapter = TvShowPagedAdapter()
        frg_video_list_recycler_view.adapter = pagedAdapter
        val viewModel = ViewModelProviders.of(activity!!, viewModelFactory).get(TvShowListViewModel::class.java)
        viewModel.tvShows?.observe(activity!!, Observer {
            pagedAdapter.submitList(it)
        })
        viewModel.initialRequestState?.observe(this, Observer {
            when (it?.status){
                Status.LOADING -> showProgress()
                Status.LOADED -> showList()
                Status.ERROR -> showError()
            }
        })
    }

    private fun showProgress(){
        frg_video_list_progress_bar.visibility = View.VISIBLE
        frg_video_list_error_msg.visibility = View.GONE
        frg_video_list_recycler_view.visibility = View.GONE
    }

    private fun showList(){
        frg_video_list_progress_bar.visibility = View.GONE
        frg_video_list_error_msg.visibility = View.GONE
        frg_video_list_recycler_view.visibility = View.VISIBLE
    }

    private fun showError(){
        frg_video_list_progress_bar.visibility = View.GONE
        frg_video_list_recycler_view.visibility = View.GONE
        frg_video_list_error_msg.visibility = View.VISIBLE
    }
}