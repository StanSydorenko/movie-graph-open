package com.viapolares.openmoviegraph.ui

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.viapolares.openmoviegraph.data.repository.TvShowRepository
import com.viapolares.openmoviegraph.ui.activity.details.TvShowDetailsViewModel
import com.viapolares.openmoviegraph.ui.activity.list.TvShowListViewModel

/**
 * Created by Stanislav Sydorenko (Stanislav.Sydorenko@gmail.com)
 */
class ViewModelFactory(private val repository: TvShowRepository): ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T = when {
        modelClass.isAssignableFrom(TvShowListViewModel::class.java) -> TvShowListViewModel(repository) as T
        modelClass.isAssignableFrom(TvShowDetailsViewModel::class.java) -> TvShowDetailsViewModel(repository) as T
        else -> throw IllegalArgumentException("UNKNOWN ViewModel class")
    }
}