package com.viapolares.openmoviegraph.ui.activity.details

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.viapolares.openmoviegraph.data.TvShowDetails
import com.viapolares.openmoviegraph.data.repository.TvShowRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Stanislav Sydorenko (Stanislav.Sydorenko@gmail.com)
 */
class TvShowDetailsViewModel(private val repository: TvShowRepository) : ViewModel(){

    private var tvShowDetails: MutableLiveData<TvShowDetails>? = null

    private var compositeDisposable = CompositeDisposable()

    fun getTvShowDetails(id: String): LiveData<TvShowDetails>?{
        if (tvShowDetails == null){
            tvShowDetails = MutableLiveData<TvShowDetails>()
            compositeDisposable.add(requestTvShowDetails(id))
        }
        return tvShowDetails
    }

    private fun requestTvShowDetails(id: String): Disposable {
        return repository.getTvShowDetails(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    tvShowDetails?.value = it
                },{
                    tvShowDetails?.value = null
                })
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        compositeDisposable.clear()
        super.onCleared()
    }
}