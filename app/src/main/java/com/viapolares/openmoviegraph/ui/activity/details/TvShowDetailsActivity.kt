package com.viapolares.openmoviegraph.ui.activity.details

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import com.viapolares.openmoviegraph.R
import com.viapolares.openmoviegraph.data.TvShowDetails
import com.viapolares.openmoviegraph.ui.ImageType
import com.viapolares.openmoviegraph.ui.ViewModelFactory
import com.viapolares.openmoviegraph.ui.loadWithGlide
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.act_video_detailes.*
import kotlinx.android.synthetic.main.act_video_details_header.*
import java.text.DecimalFormat
import javax.inject.Inject

/**
 * Created by Stanislav Sydorenko (Stanislav.Sydorenko@gmail.com)
 */
class TvShowDetailsActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_video_detailes)
        setCollapsingToolbar()

        val id = intent?.extras?.getString(VIDEO_DETAILS_ARG_KEY_ID)

        if (id == null || id.isEmpty()) {
            showError()
            return
        }

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(TvShowDetailsViewModel::class.java)
        viewModel.getTvShowDetails(id)?.observe(this, Observer {
            if (it != null){
                setTvShowDetails(it)
            } else {
                showError()
            }
        })
    }

    private fun setTvShowDetails(details: TvShowDetails) {
        act_video_details_info_layout.visibility = View.VISIBLE
        act_video_details_info_layout.animate().alpha(1f).setDuration(250L).start()
        act_video_details_progress.visibility = View.GONE
        act_video_details_error_msg.visibility = View.GONE
        details.posterPath?.let {
            act_video_details_poster.loadWithGlide(ImageType.POSTER.getUrl(it))
        }
        details.backdropPath?.let {
            act_video_details_header_image.loadWithGlide(ImageType.BACKDROP.getUrl(it), R.drawable.logo_header)
        }

        act_video_details_toolbar_title.text = details.title
        act_video_details_title.text = details.title

        act_video_details_subtitle_1.text = String.format(
                getString(R.string.act_tv_show_details_release_date), details.releaseDate)
        act_video_details_subtitle_2.text =
                String.format(getString(R.string.act_tv_show_details_episodes_seasons_number),
                        details.seasonsNumber, details.episodesNumber)
        act_video_details_subtitle_3.text =
                String.format(getString(R.string.act_tv_show_details_vote_info),
                        DecimalFormat("#.0").format(details.voteAverage), details.voteCount)

        act_video_details_overview.text = details.overview
        act_act_video_details_favorite_iv.setOnClickListener {
            it.isSelected = !it.isSelected
            act_act_video_details_favorite_anim_container.showFavoriteAnimatedView(it.isSelected)
        }
    }

    private fun showError(){
        act_video_details_error_msg.visibility = View.VISIBLE
        act_video_details_info_layout.visibility = View.GONE
        act_video_details_progress.visibility = View.GONE
    }

    private fun setCollapsingToolbar(){
        setSupportActionBar(act_video_details_toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        var isShow = false
        var totalRange = -1

        act_video_details_app_bar?.addOnOffsetChangedListener { layout, i ->
            if (totalRange == -1){
                totalRange = layout.totalScrollRange
            }
            if (layout.totalScrollRange + i < 10) {
                showTitle(true)
                isShow = true
            } else if (isShow) {
                showTitle(false)
                isShow = false
            }
        }
    }

    private fun showTitle(show: Boolean){
        val duration = resources.getInteger(android.R.integer.config_longAnimTime).toLong()
        val alpha = if (show) 1f else 0f
        act_video_details_toolbar_title.animate().alpha(alpha).setDuration(duration).start()
    }

    companion object {
        const val VIDEO_DETAILS_ARG_KEY_ID = "VIDEO_DETAILS_ARG_KEY_ID"
    }
}