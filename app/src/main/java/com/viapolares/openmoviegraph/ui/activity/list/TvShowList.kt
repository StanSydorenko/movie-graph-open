package com.viapolares.openmoviegraph.ui.activity.list

import android.arch.paging.PagedListAdapter
import android.content.Intent
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.viapolares.openmoviegraph.R
import com.viapolares.openmoviegraph.data.TvShow
import com.viapolares.openmoviegraph.ui.ImageType
import com.viapolares.openmoviegraph.ui.activity.details.TvShowDetailsActivity
import com.viapolares.openmoviegraph.ui.activity.details.TvShowDetailsActivity.Companion.VIDEO_DETAILS_ARG_KEY_ID
import com.viapolares.openmoviegraph.ui.loadWithGlide
import kotlinx.android.synthetic.main.list_item_tv_show.view.*

/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */
class TvShowPagedAdapter: PagedListAdapter<TvShow, TvShowViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TvShowViewHolder {
        return TvShowViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_tv_show, parent, false))
    }

    override fun onBindViewHolder(holder: TvShowViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<TvShow>() {
            override fun areItemsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
                return oldItem == newItem
            }
        }
    }
}

class TvShowViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
    fun bind(tvShow: TvShow){
        itemView?.list_item_tv_show_title?.text = tvShow.title
        itemView?.list_item_tv_show_vote_average?.text = tvShow.voteAverage.toString()
        tvShow.posterPath?.let {
            itemView.list_item_tv_show_poster.loadWithGlide(ImageType.POSTER.getUrl(it))
        }
        itemView?.setOnClickListener {
            val intent = Intent(it.context, TvShowDetailsActivity::class.java)
            intent.putExtra(VIDEO_DETAILS_ARG_KEY_ID, tvShow.id)
            it.context.startActivity(intent)
        }
    }
}