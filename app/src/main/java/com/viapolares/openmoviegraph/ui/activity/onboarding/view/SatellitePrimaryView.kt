package com.viapolares.openmoviegraph.ui.activity.onboarding.view

import android.animation.ValueAnimator
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import android.view.animation.AnticipateInterpolator
import android.view.animation.AnticipateOvershootInterpolator
import android.view.animation.OvershootInterpolator
import com.viapolares.openmoviegraph.R
import com.viapolares.openmoviegraph.ui.activity.onboarding.OnboardingActivity
import com.viapolares.openmoviegraph.ui.activity.onboarding.OnboardingActivity.Companion.ANGLE_STEP

/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */
class SatellitePrimaryView : AppCompatImageView, SatelliteView {

    private lateinit var params: ConstraintLayout.LayoutParams
    private lateinit var rotationAnim: ValueAnimator
    private lateinit var scaleUpAnim: ValueAnimator
    private lateinit var scaleDownAnim: ValueAnimator
    override var isCurrent = false

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (layoutParams !is ConstraintLayout.LayoutParams){
            throw IllegalStateException("SatellitePrimaryView must be child of ConstraintLayout")
        }
        params = layoutParams as ConstraintLayout.LayoutParams
        rotationAnim = createRotationAnim()
        scaleUpAnim = createScaleUpAnim2()
        scaleDownAnim = createScaleDownAnim2()
    }

    override fun onForwardClicked(isNext: Boolean) {
        rotationAnim.setFloatValues(params.circleAngle, params.circleAngle + ANGLE_STEP)
        rotationAnim.start()
        if (isNext) {
            scaleUpAnim.start()
            isCurrent = true
        } else if (isCurrent){
            scaleDownAnim.start()
            isCurrent = false
        }
    }

    override fun onBackwardClicked(isNext: Boolean) {
        rotationAnim.setFloatValues(params.circleAngle, params.circleAngle - ANGLE_STEP)
        rotationAnim.start()
        if (isNext) {
            scaleUpAnim.start()
            isCurrent = true
        } else if (isCurrent) {
            scaleDownAnim.start()
            isCurrent = false
        }
    }

    private fun createRotationAnim(): ValueAnimator {
        val anim = ValueAnimator.ofFloat(params.circleAngle)
        anim.addUpdateListener { valueAnimator ->
            params.circleAngle = valueAnimator.animatedValue as Float
            layoutParams = params
        }
        anim.duration = OnboardingActivity.DURATION_MAIN_MS
        anim.interpolator = AnticipateOvershootInterpolator(0.5f)
        return anim
    }

    private fun createScaleUpAnim2(): ValueAnimator {
        val anim = ValueAnimator.ofInt(smallSize(context), largeSize(context))
        anim.addUpdateListener { valueAnimator ->
            params.width = valueAnimator.animatedValue as Int
            params.height = valueAnimator.animatedValue as Int
            layoutParams = params
        }
        anim.startDelay = OnboardingActivity.DURATION_SCALE_DELAY_MS
        anim.duration = OnboardingActivity.DURATION_SCALE_MS
        anim.interpolator = OvershootInterpolator(2f)
        return anim
    }

    private fun createScaleDownAnim2(): ValueAnimator {
        val anim = ValueAnimator.ofInt(largeSize(context), smallSize(context))
        anim.addUpdateListener { valueAnimator ->
            params.width = valueAnimator.animatedValue as Int
            params.height = valueAnimator.animatedValue as Int
            layoutParams = params
        }
        anim.duration = OnboardingActivity.DURATION_SCALE_MS
        anim.interpolator = AnticipateInterpolator(2f)
        return anim
    }

    companion object {
        fun smallSize(context: Context) = context.resources.getDimensionPixelSize(R.dimen.satellite_primary_small)
        fun largeSize(context: Context) = context.resources.getDimensionPixelSize(R.dimen.satellite_primary_large)
    }
}