package com.viapolares.openmoviegraph.ui.activity.favorite

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.Interpolator
import android.widget.FrameLayout
import android.widget.ImageView
import com.viapolares.openmoviegraph.R
import java.util.*

/**
 * @author Stanislav Sydorenko (ssydorenko@viewster.com)
 */
@SuppressWarnings("all")
class FavoriteAnimView(context: Context, parentHeight: Int) : ImageView(context), Animator.AnimatorListener {
    private var animSet: AnimatorSet? = null

    init {
        y = parentHeight.minus(getIconWidth()).plus(context.resources.getDimension(R.dimen.favorite_view_padding))
        visibility = View.GONE
        val params = FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL)
        layoutParams = params
        prepareAnimation()
    }

    private fun prepareAnimation(){
        animSet = AnimatorSet()
        animSet?.duration = ANIM_DURATION_MS
        animSet?.playTogether(
                createAnimatorTranslationX(),
                createAnimatorTranslationY(),
                createAnimatorScaleX(),
                createAnimatorScaleY(),
                createAnimatorAlpha()
        )
        animSet?.childAnimations?.get(0)?.addListener(this)
    }

    private fun createAnimatorTranslationX(): Animator{
        val animator = ObjectAnimator.ofFloat(this, "translationX", getIconWidth().div(7))
        val cycles = Random().nextFloat().times(3f).plus(0.5f)
        animator.interpolator = Interpolator { Math.sin(2 * Math.PI * cycles * it).toFloat() }
        return animator
    }

    private fun createAnimatorTranslationY() = ObjectAnimator.ofFloat(this, "translationY", 0f)
    private fun createAnimatorScaleX() = ObjectAnimator.ofFloat(this, "ScaleX", scaleX / 2)
    private fun createAnimatorScaleY() = ObjectAnimator.ofFloat(this, "ScaleY", scaleY / 2)
    private fun createAnimatorAlpha() = ObjectAnimator.ofFloat(this, "alpha", 0f)

    private fun getIconWidth() = context.resources.getDimension(R.dimen.favorite_view_size)

    fun setInitialScale(initialScale: Float){
        scaleX = initialScale
        scaleY = initialScale
    }

    fun animateWithDelay(delay: Int) {
        animSet?.startDelay = delay.toLong()
        animSet?.start()
    }

    override fun onAnimationEnd(animation: Animator?) {
        visibility = View.GONE
        (rootView as? ViewGroup)?.removeView(this)
    }

    override fun onAnimationStart(animation: Animator?) {
        visibility = View.VISIBLE
    }

    override fun onAnimationRepeat(animation: Animator?) {
        //ignore here
    }

    override fun onAnimationCancel(animation: Animator?) {
        visibility = View.GONE
    }

    override fun onDetachedFromWindow() {
        animSet?.childAnimations?.get(0)?.removeAllListeners()
        super.onDetachedFromWindow()
    }

    companion object {
        const val ANIM_DURATION_MS = 2000L
    }
}