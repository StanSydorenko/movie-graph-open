package com.viapolares.openmoviegraph.ui.activity.favorite

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import com.viapolares.openmoviegraph.R

/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */
class FavoriteAnimViewContainer : FrameLayout {

    @Suppress("unused")
    constructor(context: Context) : super(context)

    @Suppress("unused")
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    @Suppress("unused")
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    fun showFavoriteAnimatedView(isFavorite: Boolean){
        var delay = INITIAL_DELAY
        var scaleRatio = INITIAL_SCALE
        val resId = if (isFavorite) R.drawable.ic_favorite_selected else R.drawable.ic_favorite_break_selected
        for (i in 0 until NUMBER_OF_RELEASED_VIEWS) {
            FavoriteAnimView(context, height).let {
                it.setImageResource(resId)
                it.setInitialScale(scaleRatio)
                it.animateWithDelay(delay)
                addView(it)
            }
            scaleRatio -= NEXT_SCALE_STEP
            delay += NEXT_DELAY_STEP
        }
    }

    companion object {
        const val NUMBER_OF_RELEASED_VIEWS = 3
        const val INITIAL_DELAY = 200
        const val NEXT_DELAY_STEP = 300
        const val INITIAL_SCALE = 1f
        const val NEXT_SCALE_STEP = 0.25f
    }
}
