package com.viapolares.openmoviegraph.ui.activity.onboarding.view

import android.animation.ValueAnimator
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import android.view.animation.AnticipateOvershootInterpolator
import com.viapolares.openmoviegraph.ui.activity.onboarding.OnboardingActivity

/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */
class SatelliteSecondaryView : AppCompatImageView, SatelliteView {

    private lateinit var params: ConstraintLayout.LayoutParams
    private lateinit var rotationAnim: ValueAnimator

    override var isCurrent = false // ignored

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (layoutParams !is ConstraintLayout.LayoutParams){
            throw IllegalStateException("SatellitePrimaryView must be child of ConstraintLayout")
        }
        params = layoutParams as ConstraintLayout.LayoutParams
        rotationAnim = createRotationAnim()
    }

    override fun onForwardClicked(isNext: Boolean) {
        rotationAnim.setFloatValues(params.circleAngle, params.circleAngle + 720)
        rotationAnim.start()
    }

    override fun onBackwardClicked(isNext: Boolean) {
        rotationAnim.setFloatValues(params.circleAngle, params.circleAngle - 720)
        rotationAnim.start()
    }

    private fun createRotationAnim(): ValueAnimator {
        val anim = ValueAnimator.ofFloat(params.circleAngle)
        anim.addUpdateListener { valueAnimator ->
            params.circleAngle = valueAnimator.animatedValue as Float
            layoutParams = params
        }
        anim.duration = OnboardingActivity.DURATION_MAIN_MS + ONBOARDING_SECONDARY_VIEW_DELAY_MS
        anim.interpolator = AnticipateOvershootInterpolator(0.6f)
        return anim
    }

    companion object {
        const val ONBOARDING_SECONDARY_VIEW_DELAY_MS = 100
    }
}