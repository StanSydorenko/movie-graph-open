package com.viapolares.openmoviegraph.ui.activity.onboarding

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.animation.DecelerateInterpolator
import android.widget.Scroller

/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */
class OnboardingViewPager: ViewPager {

    constructor(context: Context): super(context) {
        setCustomScroller()
    }

    constructor(context: Context, attributeSet: AttributeSet): super(context, attributeSet) {
        setCustomScroller()
    }

    override fun onInterceptTouchEvent(event: MotionEvent) = false

    override fun onTouchEvent(event: MotionEvent) = false

    private fun setCustomScroller() {
        try {
            val viewpager = ViewPager::class.java
            val scroller = viewpager.getDeclaredField("mScroller")
            scroller.isAccessible = true
            scroller.set(this, SmoothScroller(context))
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    inner class SmoothScroller(context: Context) : Scroller(context, DecelerateInterpolator()) {
        override fun startScroll(startX: Int, startY: Int, dx: Int, dy: Int, duration: Int) {
            super.startScroll(startX, startY, dx, dy, ONBOARDING_PAGER_SCROLL_DURATION_MS)
        }
    }

    companion object {
        const val ONBOARDING_PAGER_SCROLL_DURATION_MS = 700
    }
}