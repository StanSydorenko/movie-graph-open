package com.viapolares.openmoviegraph.ui.activity.list

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.viapolares.openmoviegraph.R
import kotlinx.android.synthetic.main.act_video_list.*

/**
 * Created by Stanislav Sydorenko (Stanislav.Sydorenko@gmail.com)
 */
class TvShowListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_video_list)
        setSupportActionBar(act_video_list_toolbar)

        act_video_list_view_pager.adapter = TvShowListPagerAdapter(supportFragmentManager)
        act_video_list_tab_layout.setupWithViewPager(act_video_list_view_pager)
    }
}
