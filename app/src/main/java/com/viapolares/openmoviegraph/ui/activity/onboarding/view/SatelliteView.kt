package com.viapolares.openmoviegraph.ui.activity.onboarding.view


/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */
interface SatelliteView {
    var isCurrent: Boolean
    fun onForwardClicked(isNext: Boolean)
    fun onBackwardClicked(isNext: Boolean)
}