package com.viapolares.openmoviegraph.viewmodel

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PagedList
import com.viapolares.openmoviegraph.data.TvShow
import com.viapolares.openmoviegraph.data.repository.TvShowRepository
import com.viapolares.openmoviegraph.data.repository.TvShowsPagedResult
import com.viapolares.openmoviegraph.ui.activity.list.TvShowListViewModel
import org.junit.*
import org.junit.Assert.assertEquals
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner


/**
 * Created by Stanislav Sydorenko (Stanislav.Sydorenko@gmail.com)
 */
@RunWith(MockitoJUnitRunner::class)
class TvShowListViewModelTest {

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @Test
    fun testReceivingResult() {
        val fakeTvShowResult = TvShowsPagedResult(MutableLiveData<PagedList<TvShow>>(), MutableLiveData(), MutableLiveData())
        val repository = Mockito.mock(TvShowRepository::class.java)
        Mockito.`when`(repository.getTvShowsPagedLiveData()).thenReturn(fakeTvShowResult)
        val model = TvShowListViewModel(repository)

        assertEquals(fakeTvShowResult.pagedList, model.tvShows)
        assertEquals(fakeTvShowResult.nextRequestState, model.nextRequestState)
        assertEquals(fakeTvShowResult.initialRequestState, model.initialRequestState)
    }
}