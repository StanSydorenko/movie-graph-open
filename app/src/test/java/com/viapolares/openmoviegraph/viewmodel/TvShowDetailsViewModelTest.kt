package com.viapolares.openmoviegraph.viewmodel

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.viapolares.openmoviegraph.TestObjectsCreator.createFakeTvShowDetails
import com.viapolares.openmoviegraph.data.repository.TvShowRepository
import com.viapolares.openmoviegraph.ui.activity.details.TvShowDetailsViewModel
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import org.junit.*
import org.junit.Assert.assertEquals
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.Scheduler


/**
 * Created by Stanislav Sydorenko (Stanislav.Sydorenko@gmail.com)
 */
@RunWith(MockitoJUnitRunner::class)
class TvShowDetailsViewModelTest {

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @Mock
    lateinit var repository: TvShowRepository

    private lateinit var model: TvShowDetailsViewModel

    @Before
    fun setUp() {
        model = TvShowDetailsViewModel(repository)
    }

    @Test
    fun testSuccessfulLoading() {
        val fakeId = Mockito.anyString()
        val fakeTvShow = createFakeTvShowDetails()
        Mockito.`when`(repository.getTvShowDetails(fakeId)).thenReturn(Single.just(fakeTvShow))
        val tvShowLiveData = model.getTvShowDetails(fakeId)

        assertEquals(fakeTvShow, tvShowLiveData?.value)
    }

    @Test
    fun testErrorLoading() {
        val fakeId = Mockito.anyString()
        Mockito.`when`(repository.getTvShowDetails(fakeId)).thenReturn(Single.error(Throwable()))
        val tvShowLiveData = model.getTvShowDetails(fakeId)

        assertEquals(null, tvShowLiveData?.value)
    }

    companion object {

        @JvmStatic
        @BeforeClass
        fun setUpClass() {
            val immediate = object : Scheduler() {
                override fun createWorker(): Worker {
                    return ExecutorScheduler.ExecutorWorker({ it.run() })
                }
            }

            RxJavaPlugins.setInitIoSchedulerHandler { immediate }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { immediate }
        }
    }

}