package com.viapolares.openmoviegraph

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.arch.paging.PagedList
import com.viapolares.openmoviegraph.data.TvShow
import com.viapolares.openmoviegraph.data.TvShowApi
import com.viapolares.openmoviegraph.data.paging.PagedContentSourceFactory
import com.viapolares.openmoviegraph.data.repository.TvShowRepository
import com.viapolares.openmoviegraph.data.repository.TvShowRepositoryImpl
import com.viapolares.openmoviegraph.data.repository.TvShowRepositoryImpl.Companion.INITIAL_PAGE
import io.reactivex.Single
import org.junit.*
import org.junit.Assert.*
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner


/**
 * Created by Stanislav Sydorenko (Stanislav.Sydorenko@gmail.com)
 */
@RunWith(MockitoJUnitRunner::class)
class TvShowRepositoryTest {

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @Mock
    lateinit var api: TvShowApi

    lateinit var repository: TvShowRepository

    @Before
    fun setUp() {
        repository = TvShowRepositoryImpl(api, PagedContentSourceFactory(api))
    }

    @Test
    fun testReceivingTvShowList() {
        val apiFakeResponse = TestObjectsCreator.createFakeTvShowResponse(INITIAL_PAGE)
        Mockito.`when`(api.getPopularTvShowList(INITIAL_PAGE)).thenReturn(Single.just(apiFakeResponse))

        val pagedList = getPagedList(repository.getTvShowsPagedLiveData().pagedList)
        assertEquals(apiFakeResponse.results, pagedList)
    }

    private fun getPagedList(liveData: LiveData<PagedList<TvShow>>): PagedList<TvShow> {
        val observer = TestObserver<PagedList<TvShow>>()
        liveData.observeForever(observer)
        assertNotNull(observer.value)
        return observer.value!!
    }

    private class TestObserver<T> : Observer<T> {
        var value : T? = null
        override fun onChanged(t: T?) {
            this.value = t
        }
    }

    @Test
    fun testReceivingTvShowDetails() {
        val apiFakeResponse = TestObjectsCreator.createFakeTvShowDetails()
        Mockito.`when`(api.getTvShowDetails("id")).thenReturn(Single.just(apiFakeResponse))

        assertNotNull(repository.getTvShowDetails("id").blockingGet())
        assertEquals(repository.getTvShowDetails("id").blockingGet(), apiFakeResponse)
    }
}