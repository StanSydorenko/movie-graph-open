package com.viapolares.openmoviegraph

import com.viapolares.openmoviegraph.data.TvShow
import com.viapolares.openmoviegraph.data.TvShowDetails
import com.viapolares.openmoviegraph.data.TvShowResponse

/**
 * Created by Stanislav Sydorenko (stanislav.sydorenko@gmail.com)
 */
object TestObjectsCreator {

    private const val TOTAL_RESULTS = 100
    private const val TOTAL_PAGES = 5

    fun createFakeTvShowResponse(page: Int): TvShowResponse =
            TvShowResponse(page, TOTAL_RESULTS, TOTAL_PAGES, createFakeTvShowList())

    private fun createFakeTvShowList(): List<TvShow> =
            listOf(
                    createFakeTvShow(1),
                    createFakeTvShow(2),
                    createFakeTvShow(3))

    private fun createFakeTvShow(id: Int): TvShow =
            TvShow(id.toString(),
                    "title",
                    "posterPath",
                    0F)

    fun createFakeTvShowDetails(): TvShowDetails =
            TvShowDetails("id",
                    "title",
                    "backdropPath",
                    "poster_path",
                    "overview",
                    "releaseDate",
                    0,
                    0,
                    0F,
                    0)

}